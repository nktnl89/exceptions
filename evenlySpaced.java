package com.epam.file;

/*
Given three ints, a b c, one of them is small, one is medium and one is large.
Return true if the three values are evenly spaced, so the difference between small
and medium is the same as the difference between medium and large.

evenlySpaced(2, 4, 6) → true
evenlySpaced(4, 6, 2) → true
evenlySpaced(4, 6, 3) → false
 */

import java.util.*;

public class evenlySpaced {
    public void evenlySpaced(Integer a, Integer b, Integer c){
        List<Integer> list = new ArrayList<Integer>();
        list.add(a);
        list.add(b);
        list.add(c);
        Collections.sort(list);
        if((list.get(1) - list.get(0))==(list.get(2) - list.get(1))) {
            System.out.println(true);
        }else{
            System.out.println(false);
        }
    }
}
