package com.epam.file;

/*

Given two strings, return true if either of the strings appears at the very end of the other string,
ignoring upper/lower case differences (in other words, the computation should not be "case sensitive").
Note: str.toLowerCase() returns the lowercase version of a string.


endOther("Hiabc", "abc") → true
endOther("AbC", "HiaBc") → true
endOther("abc", "abXabc") → true
 */

public class EndOther {
    public void endOther(String str1, String str2) {
        if (str1.length() >= str2.length()) {
            System.out.println(str1.toLowerCase().substring(str1.length() - str2.length(), str1.length()).equals(str2.toLowerCase()));
        } else {
            System.out.println(str2.toLowerCase().substring(str2.length()-str1.length(), str2.length()).equals(str1.toLowerCase()));
        }


    }
}
