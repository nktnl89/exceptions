package com.epam.file;

import java.util.HashMap;
import java.util.Map;

public class WordLen {
    public void wordLen(String[] arrayStr){
        Map<String,Integer> map = new HashMap<>();
        for (String word : arrayStr){
            if(map.get(word)==null){
                map.put(word,word.length());
            }
        }
    }
}
